/**
 * GemdepsController
 * @description :: Server-side logic for managing gemdeps
 */

module.exports = {
    
    bindAll:(req,res)=>{
        Gemdeps.find().then(gems=>{
            res.view("homepage",{gems})
        })   
    },
	create:(req,res)=>{
        let gem_deps = req.body['gem_deps'], gem_name = req.body['gem_name'];
        gem_deps=(typeof(gem_deps)=='string') ? JSON.parse(gem_deps) : gem_deps
        let obj={gem_name,gem_deps}
        Gemdeps.create(obj).exec((err,result)=>{
            if(err)
                res.badrequest(err)
            res.ok(result)
        })
    },
    getAllDeps:(req,res)=>{  
        let gems=req.body['gemList'];
        gems= (typeof(gems)=='string') ? JSON.parse(gems) : gems
        Gemdeps.find({'gem_name':gems}).then(allGems=>{
            let all=[]
            allGems.forEach(function(element) {
                let raw = element;
                let allowed = ['gem_name', 'gem_deps'];
                let filtered = Object.keys(raw)
                    .filter(key => allowed.includes(key))
                    .reduce((obj, key) => {
                        obj[key] = raw[key];
                        return obj;
                    }, {});
                all.push(filtered)    
            });           
            res.ok(all)
        }).catch(err=>{
            res.badrequest(err)
        })
    }
};