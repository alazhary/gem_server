module.exports = {
    getAll:(gems)=>{
            let all_deps=[];
            for (var i = 0; i < gems.length; i++) {
                let gem={'gem_name':gems[i]}
                all_deps.push(Gemdeps.findOne(gem));
            }
            return Promise.all(all_deps);
    }
}