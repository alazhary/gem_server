/**
 * Gemdeps.js
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 */

module.exports = {

  attributes: {
    gem_name:{
      type:'string'
    },
    gem_deps:{
      type:'array'
      /*
      schyma: "gem_deps": [
            {
                "linux": [
                    "deplinx1"
                ]
            },
            {
                "mac": [
                    "depmac1",
                    "depmac2"
                ]
            },
            {
                "windows": [
                    "depwww",
                    "dep2www"
                ]
            }
        ]
        */
    }
  },
  seedData:[
    {
      gem_name: 'nokogiri',
      gem_deps: [
                {
                    "linux": [
                        "zlib1g-dev",
                        "liblzma-dev"
                    ]
                }
            ]
    }
  ]
};

