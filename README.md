# Sails.js Gem_deps Server App and MongoDB

## Requirements

- [sails](http://sailsjs.org/) >= 0.10.4
- node >= 0.10.0
- MongoDB

## Setup

1. Clone the repository `git clone https://gitlab.com/alazhary/gem_server.git`
1. Navigate into the directory `cd gem_server`
1. Run `npm install`  to install the dependencies
1. Set up your MongoDB settings `config/connections.js`
1. Start your MongoDB from the command line with `sudo mongod`
1. Start sails with `sails lift`
1. Open `http://localhost:1337` in your browser

## RESTful API 

-All CRUD operations and REST APIs already working in the web service ... 

-to fetch collection of dependencies (system libs) needed to install
    
```routes.js
  'post /gemdeps/getMyDeps':'GemdepsController.getAllDeps'
```

## mongodb schyma
```
>  use gem_sysDeps
```
switched to db gem_sysDeps
```
> db.gemdeps.find().pretty()
{
	"_id" : ObjectId("594ac5e81b6bf970d88b4003"),
	"gem_name" : "gem_ex",
	"gem_deps" : [
		{
			"linux" : [
				"deplinx1"
			]
		},
		{
			"mac" : [
				"depmac1",
				"depmac2"
			]
		},
		{
			"windows" : [
				"depwin1",
				"dep2win2"
			]
		}
	],
	"createdAt" : ISODate("2017-06-21T19:15:52.136Z"),
	"updatedAt" : ISODate("2017-06-21T19:15:52.136Z")
}
```


## License

The MIT License (MIT)

Copyright (c) 2017 Mohammed AlAzhary

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
