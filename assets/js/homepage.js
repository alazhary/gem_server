$(function(){

    var rows=$("tbody>tr").length
  $("#addGem").click(function(e){
    let gem_name=$("#gem_name-text").val();
    let linux=($("#linux_libs-text").val()) ? JSON.parse($("#linux_libs-text").val()) : "";
    let mac=($("#mac_libs-text").val()) ? JSON.parse($("#mac_libs-text").val()) : "";
    let windows=($("#windows_libs-text").val()) ? JSON.parse($("#windows_libs-text").val()) : "";
    let gem_deps=[];
    (linux) ? gem_deps.push({linux}):"";
    (mac) ? gem_deps.push({mac}):"";
    (windows) ? gem_deps.push({windows}):"";
    console.log({gem_name, gem_deps});

    if (gem_name && gem_deps.length>=1) {
        $.ajax({
            url: "/gemdeps/create",
            method: "post" ,
            data: {gem_name,gem_deps},
            success: (gem)=>{
                console.log(gem)
                $("table>tbody").append(`<tr>
                    <td>`+(rows+1)+`</td>
                    <td>`+((gem.gem_name)    ?    gem.gem_name             : "---" )+`</td>
                    <td>`+((gem.gem_deps[0]) ?  ( (gem.gem_deps[0].linux)   ? gem.gem_deps[0].linux : "---")  : "---" )+`</td>
                    <td>`+((gem.gem_deps[1]) ?  ( (gem.gem_deps[1].mac)     ? gem.gem_deps[1].mac : ( (gem.gem_deps[0].mac) ? gem.gem_deps[0].mac : "---") )  : ( (gem.gem_deps[0].mac) ? gem.gem_deps[0].mac : "---") )+`</td>
                    <td>`+((gem.gem_deps[2]) ?  ( (gem.gem_deps[2].windows) ? gem.gem_deps[2].windows : "---")  : ((gem.gem_deps[1] && gem.gem_deps[1].windows) ? gem.gem_deps[1].windows : ( (gem.gem_deps[0].windows) ? gem.gem_deps[0].windows:"---") ) )+`</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                </tr>`)
                rows++;
            },
            error:(err)=>{
                console.log(err)
            } 
        })//end ajax
    }else{
        alert("Missing some fields!")
    }
  });//end add Gem 


})